const redis = require('ioredis');

const config = {
    port: 6379,          // Redis port
    host: 'localhost',   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    password: '',
    db: 0
}

const newredis = new redis(config);

module.exports = newredis;