/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-03 21:46:21
 * @LastEditTime : 2020-01-13 15:21:56
 * @LastEditors  : Please set LastEditors
 */
// 导入compression-webpack-plugin
const CompressionWebpackPlugin = require('compression-webpack-plugin')
// 定义压缩文件类型
const productionGzipExtensions = ['js', 'css']
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const PrerenderSPAPlugin = require('prerender-spa-plugin');
const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;

const baseUrl = process.env.NODE_ENV === 'production' ? '@/examples/assets' : '@/examples/assets';
const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  //...
  publicPath: './', //打包的默认路径
  pages: {
    index: {
      // page 的入口
      entry: 'examples/main.js',
      // 模板来源
      template: 'public/index.html',
      // 输出文件名
      filename: 'index.html'
    }
    // page1: {
    //   // 应用入口配置，相当于单页面应用的main.js，必需项
    //   entry: 'examples/main.js',
    //   // 应用的模版，相当于单页面应用的public/index.html，可选项，省略时默认与模块名一致
    //   template: 'public/index.html',
    //   // 编译后在dist目录的输出文件名，可选项，省略时默认与模块名一致
    //   filename: 'index.html',
    // },
    // page2: {
    //   entry: 'examples/modules/article/article.js',
    //   template: 'public/article.html',
    //   filename: 'article.html',
    // }
  },
  css: {
    // 启用 CSS modules
    modules: false,
    // 是否使用css分离插件
    extract: false,
    // 开启 CSS source maps，一般不建议开启
    sourceMap: false,
    loaderOptions: {
      less: {
        javascriptEnabled: true, //less 配置
      },
      sass: {
        javascriptEnabled: true,
        data: `$baseUrl: "${baseUrl}";`
      }
      // postcss: {//转化rem单位
      //   plugins: [
      //     require('postcss-px2rem')({
      //       remUnit: 48
      //     })
      //   ]
      // }
    },
    modules: false // 启用 CSS modules for all css / pre-processor files.
  }, //强制关联Css样式
  productionSourceMap: false,
  //代码审查
  lintOnSave: false,
  configureWebpack: {
    //警告 webpack 的性能提示
    performance: {
      hints: 'warning',
      //入口起点的最大体积
      maxEntrypointSize: 50000000,
      //生成文件的最大体积
      maxAssetSize: 30000000,
      //只给出 js 文件的性能提示
      assetFilter: function (assetFilename) {
        return assetFilename.endsWith('.js');
      }
    },

  },
  // lib模式图片限制
  // chainWebpack: config => {
  //     config.module
  //         .rule('images')
  //         .use('url-loader')
  //         .loader('url-loader')
  //         .tap(options => Object.assign(options, {
  //             limit: 102400
  //     }))
  // },
  //图片压缩
  chainWebpack: config => {
    config.module
      .rule('images')
      .use('image-webpack-loader')
      .loader('image-webpack-loader')
      .options({
        bypassOnDebug: true
      }).end()

  },
  //全局路径
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', resolve('/'))
  },
  //TS
  configureWebpack: {
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".json"]
    },
    module: {
      rules: [{
        test: /.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/.vue$/],
        }
      }]
    }
  },
  //页面预渲染
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production')
      return {
        plugins: [
          new PrerenderSPAPlugin({
            // 生成文件的路径，也可以与webpakc打包的一致。
            // 下面这句话非常重要！！！
            // 这个目录只能有一级，如果目录层次大于一级，在生成的时候不会有任何错误提示，在预渲染的时候只会卡着不动。
            staticDir: path.join(__dirname, 'dist'),
            // 对应自己的路由文件，比如a有参数，就需要写成 /a/param1。
            routes: ['/anli'],
            // 这个很重要，如果没有配置这段，也不会进行预编译
            renderer: new Renderer({
              inject: {
                foo: 'bar'
              },
              headless: false,
              // 在 main.js 中 document.dispatchEvent(new Event('render-event'))，两者的事件名称要对应上。
              renderAfterDocumentEvent: 'render-event'
            })
          }),

          //Gzip压缩需要后端支持
          new CompressionWebpackPlugin({
            filename: '[path].gz[query]',
            algorithm: 'gzip',
            test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
            threshold: 10240,
            minRatio: 0.8
          })
        ],
      };
  }
}