/*
 Navicat Premium Data Transfer

 Source Server         : mo
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : mo

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 14/12/2019 12:21:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mo_anli_area
-- ----------------------------
DROP TABLE IF EXISTS `mo_anli_area`;
CREATE TABLE `mo_anli_area`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anli_Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `anli_Author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `anli_Jianjie` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `anli_Url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isLock` int(11) NULL DEFAULT NULL,
  `anli_imgUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_anli_area
-- ----------------------------
INSERT INTO `mo_anli_area` VALUES (1, '和之梦', '竹内亮', '拜访住在世界各地的中国人，和住在中国的外国人。带你领略各地的风土人情。', 'https://space.bilibili.com/17244618/', 0, 'hangzhou.aliyuncs.com/user_logo.png', '2019-12-09 09:45:38', '2019-12-09 09:45:42');
INSERT INTO `mo_anli_area` VALUES (2, 'h5', '菜鸟', 'html', 'https://www.runoob.com/html/html5-intro.html', 0, 'hangzhou.aliyuncs.com/user_logo.png', '2019-12-09 13:20:46', '2019-12-09 13:20:50');

-- ----------------------------
-- Table structure for mo_anli_area_love
-- ----------------------------
DROP TABLE IF EXISTS `mo_anli_area_love`;
CREATE TABLE `mo_anli_area_love`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anli_Id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isDel` int(11) NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_anli_area_love
-- ----------------------------
INSERT INTO `mo_anli_area_love` VALUES (13, '1', '1', 0, '2019-12-09 13:26:36', '2019-12-09 13:26:36');
INSERT INTO `mo_anli_area_love` VALUES (15, '2', '1', 0, '2019-12-11 14:43:32', '2019-12-11 14:43:32');
INSERT INTO `mo_anli_area_love` VALUES (16, '1', '24', 0, '2019-12-13 09:16:15', '2019-12-13 09:16:15');

-- ----------------------------
-- Table structure for mo_anli_dongdan
-- ----------------------------
DROP TABLE IF EXISTS `mo_anli_dongdan`;
CREATE TABLE `mo_anli_dongdan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dongForm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isLock` int(11) NOT NULL,
  `love` int(11) NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_anli_dongdan
-- ----------------------------
INSERT INTO `mo_anli_dongdan` VALUES (1, '1', '1111', 0, 0, '2019-12-04 14:12:59', '2019-12-04 14:12:59');
INSERT INTO `mo_anli_dongdan` VALUES (2, '1', 'dasda', 0, 0, '2019-12-04 14:14:32', '2019-12-04 14:14:32');
INSERT INTO `mo_anli_dongdan` VALUES (3, '1', '大声道啊阿打算撒按时按时哒阿萨斯啊萨达啊', 0, 0, '2019-12-04 14:14:41', '2019-12-04 14:14:41');
INSERT INTO `mo_anli_dongdan` VALUES (12, '1', '1231', 0, 0, '2019-12-10 14:34:38', '2019-12-10 14:34:38');
INSERT INTO `mo_anli_dongdan` VALUES (27, '1', '123', 0, 0, '2019-12-10 15:01:26', '2019-12-10 15:01:26');
INSERT INTO `mo_anli_dongdan` VALUES (28, '1', '123', 0, 0, '2019-12-10 15:02:42', '2019-12-10 15:02:42');
INSERT INTO `mo_anli_dongdan` VALUES (29, '1', '123', 0, 0, '2019-12-10 15:02:48', '2019-12-10 15:02:48');
INSERT INTO `mo_anli_dongdan` VALUES (30, '1', '123', 0, 0, '2019-12-10 15:03:31', '2019-12-10 15:03:31');
INSERT INTO `mo_anli_dongdan` VALUES (31, '1', '123', 0, 0, '2019-12-10 15:03:54', '2019-12-10 15:03:54');
INSERT INTO `mo_anli_dongdan` VALUES (32, '1', '123', 0, 0, '2019-12-10 15:04:56', '2019-12-10 15:04:56');
INSERT INTO `mo_anli_dongdan` VALUES (33, '1', '1', 0, 0, '2019-12-10 15:06:42', '2019-12-10 15:06:42');
INSERT INTO `mo_anli_dongdan` VALUES (52, '1', '1', 0, 0, '2019-12-10 15:31:13', '2019-12-10 15:31:13');
INSERT INTO `mo_anli_dongdan` VALUES (53, '2', '1', 0, 0, '2019-12-10 15:33:47', '2019-12-10 15:33:47');
INSERT INTO `mo_anli_dongdan` VALUES (54, '2', '2', 0, 0, '2019-12-10 15:34:07', '2019-12-10 15:34:07');
INSERT INTO `mo_anli_dongdan` VALUES (55, '1', '嗨呀', 0, 0, '2019-12-10 15:34:31', '2019-12-10 15:34:31');
INSERT INTO `mo_anli_dongdan` VALUES (56, '1', '21', 0, 0, '2019-12-10 15:49:40', '2019-12-10 15:49:40');
INSERT INTO `mo_anli_dongdan` VALUES (57, '1', '哇咔咔', 0, 0, '2019-12-11 14:13:37', '2019-12-11 14:13:37');
INSERT INTO `mo_anli_dongdan` VALUES (58, '2', '哇咔咔', 0, 0, '2019-12-12 14:24:07', '2019-12-12 14:24:07');

-- ----------------------------
-- Table structure for mo_article
-- ----------------------------
DROP TABLE IF EXISTS `mo_article`;
CREATE TABLE `mo_article`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `article` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `owner` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ishow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `islock` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `iclass` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imoshi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ijianjie` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `igaiyao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ititle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isee` int(255) NOT NULL,
  `artid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_article
-- ----------------------------
INSERT INTO `mo_article` VALUES (1, '<p>阿达</p><p>按时a</p>', '1', '1', '0', '吐槽', '1', '阿萨德', '按时', '实打实', 9, '-1', '2019-11-18 13:19:41', '2019-12-12 13:50:12');
INSERT INTO `mo_article` VALUES (2, '<p>阿达</p><p>按时a</p><p><br></p><p>d阿萨德</p><p><br></p><p><br></p><p>阿萨德a</p><p>按时as</p><p>按时</p>', '1', '1', '0', '吐槽', '1', '阿达', '打算萨达', '按时大大说', 52, '-1', '2019-11-18 13:19:43', '2019-12-04 08:36:56');
INSERT INTO `mo_article` VALUES (4, '<p>大大</p><p><br></p><p><br></p><p>实打实</p><p><br></p><p>的按时</p>', '1', '1', '0', '二次元', '1', '打算', '是哒 ', '萨达', 15, '-1', '2019-11-21 11:10:37', '2019-12-04 08:35:43');
INSERT INTO `mo_article` VALUES (5, '<p><a href=\"https://my.oschina.net/u/2663968/blog/3120060\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(170, 0, 0);\"><strong>面试了阿里、美团与滴滴后，我有了几个重大发现…&gt;&gt;&gt;&nbsp;</strong></a><img src=\"https://www.oschina.net/img/hot3.png\"></p><p>本文作者：<span style=\"color: rgb(51, 51, 51);\">Craig Kerstiens ，目前在负责&nbsp;</span><a href=\"https://www.citusdata.com/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">@citusdata</a>&nbsp;的云团队。Citus 将 Postgres 扩展为可水平扩展的分布式数据库。本文是他之前一个好友采访他的记录（英文原文请看<a href=\"http://www.craigkerstiens.com/2019/11/13/postgres-interview-from-art-of-postgresql/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">这里</a>）。</p><p><img src=\"http://f.cl.ly/items/0d0R410q0q3Z1g0n0P0a/541575_10151640210955132_1516824014_n.jpg\"></p><p>以下是采访内容：</p><p><span style=\"color: rgb(51, 51, 51);\">我之前时常跟各种开发者共进晚餐，基本上都认为开发者（包括他们自己）可能会有些自以为是。例如，有人说：“我爱Postgres，但我不知道为什么。” 我想他们应该是坐错了餐桌，以 Postgres 为例。。。经常有人问&nbsp;</span><a href=\"http://www.craigkerstiens.com/2017/04/30/why-postgres-five-years-later/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">我为什么选择 Postgres</a><span style=\"color: rgb(51, 51, 51);\">。</span></p><p><span style=\"color: rgb(51, 51, 51);\">实际上，一年多以前，好朋友 Dimitri Fontaine 在写一本 Postgres 的书，他询问是否可以采访我。我一直说他们缺少关于 Postgres 的好书，不过这个好友做了一件非常好的事，这本书提供针对开发人员（也包括 DBA）提供了很好的指南。但他希望能做得更好。</span></p><p><span style=\"color: rgb(51, 51, 51);\">接下来是本书访谈的摘录。</span></p><p><strong style=\"color: rgb(51, 51, 51);\">介绍</strong></p><p><strong style=\"color: rgb(51, 51, 51);\">自从被 Microsoft 收购以来，</strong><span style=\"color: rgb(51, 51, 51);\">&nbsp;Craig 领导了</span><a href=\"https://www.twitter.com/citusdata\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">@citusdata</a><span style=\"color: rgb(51, 51, 51);\">&nbsp;云团队&nbsp;，该团队</span><strong style=\"color: rgb(51, 51, 51);\">现在正在运行 Azure Postgres 的产品</strong><span style=\"color: rgb(51, 51, 51);\">。Citus 将 Postgres 扩展为可水平扩展的分布式数据库。如果您有一个数据库，尤其是Postgres，则需要扩展到更多的节点（通常在100GB以上），Craig 总是很乐意聊天，看看 Citus 是否可以提供帮助。</span></p><p><span style=\"color: rgb(51, 51, 51);\">Craig 之前在 @heroku PaaS 平台上工作了很多年，这个平台消耗了大量的 IT 资源，使得开发人员专注于构建功能和增加价值。Craig 在 Heroku 的大部分时间都花在了 Heroku Data 的产品和营销上。</span></p><p><strong style=\"color: rgb(51, 51, 51);\">您认为 PostgreSQL 开源项目和生态系统的扩展有多重要？</strong></p><blockquote><em>对我而言，扩展API和不断扩展的扩展生态系统可能是过去10年 Postgres 的最大进步。扩展功能使 Postgres 可以从传统的关系数据库扩展到更多的数据平台。无论是 hstore 中的初始 NoSQL 数据类型（不包括XML），还是GIS地理空间中丰富的功能集，或者近似算法（例如HyperLogLog或TopN），您都可以通过扩展本身将Postgres带入新的领域。</em></blockquote><blockquote><em>扩展功能使得数据库核心本身可以以较慢的速度发展。核心中的每个新功能都意味着它必须经过全面测试和安全。这并不是说扩展没有，而是可以存在于核心之外，这样才使得这些扩展的功能可以快速增长。</em></blockquote><p><strong style=\"color: rgb(51, 51, 51);\">您最喜欢的 PostgreSQL 扩展是什么，为什么？</strong></p><blockquote><em>我最喜欢的三个扩展是：</em></blockquote><ol><li><em>pg_stat_statements</em></li><li><em>Citus</em></li><li><em>HyperLogLog</em></li></ol><blockquote><code><em>pg_stat_statements</em></code><em>对于应用程序开发人员而言，它无疑是最强大的扩展，无需深入了解数据库内部知识即可获得洞察力以优化其数据库。对于许多应用程序开发人员来说，数据库是一个黑匣子，而</em><code><em>pg_stat_statements</em></code><em>使用 AI 的强大基础，可以让您的数据库能够得到及时改进。</em></blockquote><blockquote><em>Citus：我当然会因为在那儿工作而有偏见，但是在加入之前，我关注了Citus和pg_shard长达三年。Citus将Postgres变成了一个水平可扩展的数据库。在后台进行分片，但应用程序开发人员不必考虑或了解这种复杂性。有了Citus，Postgres可以处理比以往更大的负荷量，因为以前的Postgres都被限制在一个盒子或过于复杂的架构中。</em></blockquote><blockquote><em>HyperLogLog：我要坦白。在某种程度上，我只是喜欢这么说：当您了解算法本身后，你会觉得自己很傻逼。“ K最小值，可观察的位模式，随机平均，谐波平均。”我的意思是当我们在用这些特性时候，干嘛一定非得很熟悉这些算法本身呢？简而言之，HyperLogLog 足够独特，而且占用的存储空间很小。如果您要构建类似Web分析的工具，那么HyperLogLog无疑是一个不错的选择。</em></blockquote><p><strong style=\"color: rgb(51, 51, 51);\">您通常如何找到可能需要的扩展？或者说，您怎么知道您可能首先需要PostgreSQL扩展？</strong></p><blockquote><a href=\"https://pgxn.org/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\"><em>pgxn.org</em></a><em>和github是我的两个建议。尽管Google往往也表现良好。当然，我还通过</em><a href=\"https://postgresweekly.com/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\"><em>PostgresWeekly.com</em></a><em>来了解最新消息。</em></blockquote><blockquote><em>实际上我常常不总是意识到自己需要一个。我搜索我要解决的问题并发现它。我可能永远不会搜索HyperLogLog，但是搜索Postgres近似计数或近似非重复的时候就能很快找到它。</em></blockquote><p><strong style=\"color: rgb(51, 51, 51);\">您的应用程序代码库现在依赖某些PostgreSQL扩展才能运行时，您是否会想到任何弊端？例如，我可能需要扩展在云和SaaS产品中的可用性。</strong></p><blockquote><em>这个要看具体是什么扩展，有些扩展问题比较多，而有些扩展则更加成熟。许多主要的云提供商都支持一系列扩展，但它们不支持更多新的扩展。如果支持它，那么使用上就不会有太大的弊端。如果没有，您需要权衡自己运行和管理Postgres的成本，以及该扩展所提供的价值。就像管理所有事物与不管理所有事物一样，这里需要权衡取舍，您需要确定哪一个适合您。</em></blockquote><p><em style=\"color: rgb(51, 51, 51);\">如果您正在寻找有关Postgres的更深入的资源，我建议您读《</em><a href=\"https://theartofpostgresql.com/?affiliate=cek\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\"><em>PostgreSQL的艺术</em></a><em style=\"color: rgb(51, 51, 51);\">》一书。它是由一个私人朋友设计的，旨在从开发人员的角度为Postgres创建权威指南。如果使用代码CRAIG15，您还将获得15％的折扣</em></p>', '1', '1', '0', '技术', '1', '发大多数', '对方是否是是送的实打实', '萨达撒打算啊', 10, '-1', '2019-11-22 10:11:02', '2019-12-12 14:09:13');
INSERT INTO `mo_article` VALUES (7, '<p><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span><span style=\"color: rgb(204, 0, 0);\">菜鸟</span><span style=\"color: rgb(51, 51, 51);\">是一个网络用语,一指新手,二指在某方面应用水平较低者。某个人刚刚加入某个团队,或某个组织刚刚进入某个行业,适应环境,接受新的事物,都是需要一个</span></p>', '1', '1', '0', '二次元', '1', '鼎折覆餗', '大萨达按时', '嗒嗒滴', 9, '-1', '2019-11-22 10:14:31', '2019-12-07 10:27:34');
INSERT INTO `mo_article` VALUES (8, '<p>以往的软件应用都是单块应用，随着用户和流量增加，单块应用无法支持，而且复杂的单块应用也难以维护、难以测试。微服务架构希望把服务拆分打包装进容器来解决这些问题，然而大家发现微服务的运维工作量不是简单地按照服务的数量线性增加，而是按照服务数量的平方增加。可想而知，如果不想办法降低运维成本，微服务就成了不切实际的空中楼阁。那么如何让微服务真正落地呢？今天我们就来聊聊这个话题。</p><p>OSCHINA 本期高手问答<strong>（11 月 26&nbsp;日 - 12&nbsp;月 03&nbsp;日）</strong>将围绕<strong style=\"color: rgb(41, 128, 185);\">「微服务运维」</strong>展开讨论，问答范围可包括但不限于：<strong>DevOps 到底是什么；实施 DevOps 所面临的挑战；DevOps 与微服务的关系</strong>等等。有其他相关的问题，也欢迎提问。为此，我们邀请到了畅销书《微服务运维实战》的译者汪欣&nbsp;<a href=\"https://my.oschina.net/u/4291775\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">@太空行走</a>&nbsp;老师。</p><p><strong>嘉宾简介</strong></p><p>汪欣，《微服务运维实践》译者。吉林大学计算机专业，有二十年的软件开发经验，多年担任知名外企系统架构师。熟悉软件开发流程，项目管理和架构设计。</p><p><img src=\"https://oscimg.oschina.net/oscnet/ad6c5328ab20f5ee4b5426b9baec5a34f59.jpg\" height=\"525\" width=\"500\"></p><p>为了鼓励踊跃提问，<strong>华中科技大学出版社</strong>会在问答结束后从提问者中抽取&nbsp;<strong>5&nbsp;名</strong>幸运会员赠予<strong style=\"color: rgb(211, 84, 0);\">《微服务运维实战（第二卷）》</strong>一书。</p><p><img src=\"https://oscimg.oschina.net/oscnet/d8b1d4a364351cdf5a2526ce22a57e5146e.png\" height=\"484\" width=\"500\"></p><p>购书地址：<a href=\"http://mrw.so/50bkUX\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">http://mrw.so/50bkUX</a>；<a href=\"http://mrw.so/4pkNwg\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">http://mrw.so/4pkNwg</a></p><p><strong style=\"color: rgb(22, 160, 133);\">OSChina 高手问答一贯的风格，不欢迎任何与主题无关的讨论和喷子。</strong></p><p>下面欢迎大家向汪欣老师&nbsp;<a href=\"https://my.oschina.net/u/4291775\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(65, 131, 196);\">@太空行走</a>&nbsp;&nbsp;积极提问，直接回帖提问即可。</p>', '1', '1', '0', '技术', '1', '阿打算哒', '阿萨德按时的', '大叔大婶', 7, '-1', '2019-11-26 08:41:12', '2019-12-06 11:22:44');
INSERT INTO `mo_article` VALUES (9, '<p>大叔大婶多a</p><p><br></p><p><br></p><p>撒大声地萨达as哒的</p><p>as哒打算啊大声道阿萨德daaas</p><p>阿萨德as啊大飒飒</p><p>阿萨德as</p><p><br></p><p><br></p><p>阿萨德萨达as</p><p><br></p><p>阿打算啊按时哒阿萨德啊</p><p><br></p><p>1111111111111111111111111111111</p>', '1', '1', '0', '二次元', '1', '大大大', '阿萨德我去我去法国微软', '为微软', 11, '-1', '2019-11-28 10:22:33', '2019-12-13 09:20:09');
INSERT INTO `mo_article` VALUES (10, '<p>大时代莽枯工在工基</p><p>我打</p><p>左右大苏打</p><p><br></p><p>基本面苛</p><p>大苏打械基苛苛基本面基本面</p><p>莽工有东西基本面基本面基本面苛有东西有东西基本面在</p><p>厅基</p><p>苛大时代</p><p><br></p><p>工莽枯 基本面苛基本面苛</p><p><br></p><p><br></p><p>有东西基本面基本面苛有东西</p><p><br></p><p>有东西苛苛左</p>', '1', '1', '0', '吐槽,技术', '1', '大苏打基本面', '苛工 大时代苛厅枯无可奈何的士速递枯', '你好，之华', 6, '-1', '2019-12-06 13:34:41', '2019-12-13 10:18:30');
INSERT INTO `mo_article` VALUES (11, '<p>dadadasdasdasdasda</p>', '2', '1', '0', '二次元', '1', '11', '发阿斯顿阿萨德阿萨德阿萨德按时阿萨德阿萨德阿萨德按时', 'da', 6, '-1', '2019-12-10 15:51:18', '2019-12-13 09:20:19');
INSERT INTO `mo_article` VALUES (12, '<p>差撒大声地打算打算是否 </p>', '1', '1', '0', '技术', '1', '大萨达', '是的撒', '佛挡杀佛是', 1, '-1', '2019-12-12 14:08:05', '2019-12-12 14:08:23');
INSERT INTO `mo_article` VALUES (13, '<p>大大大woqu</p>', '1', '1', '1', '技术', '1', '1231', '你不懂我的心', '123', 0, '-1', '2019-12-12 16:17:07', '2019-12-13 09:20:04');

-- ----------------------------
-- Table structure for mo_article_love
-- ----------------------------
DROP TABLE IF EXISTS `mo_article_love`;
CREATE TABLE `mo_article_love`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `articleId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isDel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_article_love
-- ----------------------------
INSERT INTO `mo_article_love` VALUES (29, '1', '5', '0', '2019-11-23 09:38:24', '2019-11-23 09:38:24');
INSERT INTO `mo_article_love` VALUES (30, '1', '1', '0', '2019-11-23 09:38:27', '2019-11-23 09:38:27');
INSERT INTO `mo_article_love` VALUES (31, '1', '2', '0', '2019-11-23 09:38:28', '2019-11-23 09:38:28');
INSERT INTO `mo_article_love` VALUES (32, '1', '4', '0', '2019-11-23 09:38:29', '2019-11-23 09:38:29');
INSERT INTO `mo_article_love` VALUES (33, '1', '8', '0', '2019-11-27 13:12:05', '2019-11-27 13:12:05');
INSERT INTO `mo_article_love` VALUES (34, '1', '7', '0', '2019-11-27 13:12:08', '2019-11-27 13:12:08');

-- ----------------------------
-- Table structure for mo_review
-- ----------------------------
DROP TABLE IF EXISTS `mo_review`;
CREATE TABLE `mo_review`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ownerId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `articleId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `reviewTo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `zan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `articleId`(`articleId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 286 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_review
-- ----------------------------
INSERT INTO `mo_review` VALUES (150, '111', '1', '1', '0', '', '0', '2019-11-18 13:21:31', '2019-11-18 13:21:31');
INSERT INTO `mo_review` VALUES (151, '阿萨德', '1', '1', '0', '', '0', '2019-11-18 13:21:44', '2019-11-18 13:21:44');
INSERT INTO `mo_review` VALUES (185, 'helloworld！', '1', '2', '0', '', '0', '2019-11-18 14:26:58', '2019-11-18 14:26:58');
INSERT INTO `mo_review` VALUES (189, '哒', '1', '1', '0', '', '0', '2019-11-18 14:28:29', '2019-11-18 14:28:29');
INSERT INTO `mo_review` VALUES (252, '哒阿达', '2', '1', '0', '', '0', '2019-11-19 10:51:20', '2019-11-19 10:51:20');
INSERT INTO `mo_review` VALUES (254, '随缘了', '1', '2', '0', '', '0', '2019-11-20 13:33:42', '2019-11-20 13:33:42');
INSERT INTO `mo_review` VALUES (256, '只有我吗\n', '1', '2', '0', '', '0', '2019-11-21 09:23:53', '2019-11-21 09:23:53');
INSERT INTO `mo_review` VALUES (260, '萨达', '1', '2', '0', '', '0', '2019-11-21 09:27:41', '2019-11-21 09:27:41');
INSERT INTO `mo_review` VALUES (261, '啊实打实大', '1', '2', '0', '', '0', '2019-11-21 09:28:17', '2019-11-21 09:28:17');
INSERT INTO `mo_review` VALUES (263, '啊大大阿达', '2', '2', '0', '', '0', '2019-11-21 09:28:54', '2019-11-21 09:28:54');
INSERT INTO `mo_review` VALUES (264, '大大', '1', '4', '0', '', '0', '2019-11-21 11:10:57', '2019-11-21 11:10:57');
INSERT INTO `mo_review` VALUES (265, '今天天气不错', '1', '1', '0', '', '0', '2019-11-22 08:26:33', '2019-11-22 08:26:33');
INSERT INTO `mo_review` VALUES (266, '哒', '1', '5', '0', '', '0', '2019-11-23 08:34:19', '2019-11-23 08:34:19');
INSERT INTO `mo_review` VALUES (267, '第一', '4', '7', '0', '', '0', '2019-11-23 15:59:45', '2019-11-23 15:59:45');
INSERT INTO `mo_review` VALUES (268, '@别动我结菜>大大大阿萨德阿萨德阿萨德啊', '1', '1', '0', '1', '0', '2019-11-26 08:39:51', '2019-11-26 08:39:51');
INSERT INTO `mo_review` VALUES (269, '我是第一', '1', '8', '0', '', '0', '2019-11-26 08:42:00', '2019-11-26 08:42:00');
INSERT INTO `mo_review` VALUES (270, '哦吼', '1', '4', '0', '', '0', '2019-11-28 09:37:20', '2019-11-28 09:37:20');
INSERT INTO `mo_review` VALUES (271, '111', '1', '9', '0', '', '0', '2019-11-28 10:26:20', '2019-11-28 10:26:20');
INSERT INTO `mo_review` VALUES (272, '啊实打实大12312313', '1', '9', '0', '', '0', '2019-11-28 10:56:52', '2019-11-28 10:56:52');
INSERT INTO `mo_review` VALUES (273, '滚滚滚', '1', '8', '0', '', '0', '2019-12-03 08:53:49', '2019-12-03 08:53:49');
INSERT INTO `mo_review` VALUES (274, '@郭智障>你来了？', '1', '2', '0', '2', '0', '2019-12-03 14:14:43', '2019-12-03 14:14:43');
INSERT INTO `mo_review` VALUES (275, '俺老孙到此一游！', '1', '9', '0', '', '0', '2019-12-05 14:57:24', '2019-12-05 14:57:24');
INSERT INTO `mo_review` VALUES (276, '不错哟', '1', '10', '0', '', '0', '2019-12-06 13:35:30', '2019-12-06 13:35:30');
INSERT INTO `mo_review` VALUES (277, '啦啦啦', '1', '1', '0', '', '0', '2019-12-06 15:06:47', '2019-12-06 15:06:47');
INSERT INTO `mo_review` VALUES (278, '牛逼', '1', '7', '0', '', '0', '2019-12-06 15:07:06', '2019-12-06 15:07:06');
INSERT INTO `mo_review` VALUES (279, 'haojiubujian', '2', '5', '0', '', '0', '2019-12-10 15:52:18', '2019-12-10 15:52:18');
INSERT INTO `mo_review` VALUES (280, '哇靠', '1', '10', '0', '', '0', '2019-12-12 13:52:40', '2019-12-12 13:52:40');
INSERT INTO `mo_review` VALUES (281, '1', '1', '12', '0', '', '0', '2019-12-12 14:08:28', '2019-12-12 14:08:28');
INSERT INTO `mo_review` VALUES (282, '啦啦啦啦啦啦啦', '2', '5', '0', '', '0', '2019-12-12 14:09:22', '2019-12-12 14:09:22');
INSERT INTO `mo_review` VALUES (283, '1111', '2', '10', '0', '', '0', '2019-12-12 14:09:32', '2019-12-12 14:09:32');
INSERT INTO `mo_review` VALUES (284, '哇靠', '24', '10', '0', '', '0', '2019-12-13 09:16:34', '2019-12-13 09:16:34');
INSERT INTO `mo_review` VALUES (285, '不错', '1', '11', '0', '', '0', '2019-12-13 09:20:25', '2019-12-13 09:20:25');

-- ----------------------------
-- Table structure for mo_score_log
-- ----------------------------
DROP TABLE IF EXISTS `mo_score_log`;
CREATE TABLE `mo_score_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `scoreFrom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `letter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_score_log
-- ----------------------------
INSERT INTO `mo_score_log` VALUES (14, '1', '1', 'article', '2', '2019-11-19 09:10:37', '2019-11-19 09:10:37');
INSERT INTO `mo_score_log` VALUES (15, '1', '1', 'article', '2', '2019-11-19 09:11:40', '2019-11-19 09:11:40');
INSERT INTO `mo_score_log` VALUES (16, '1', '1', 'article', '2', '2019-11-19 09:12:44', '2019-11-19 09:12:44');
INSERT INTO `mo_score_log` VALUES (19, '1', '1', 'article', '2', '2019-11-19 10:12:07', '2019-11-19 10:12:07');
INSERT INTO `mo_score_log` VALUES (20, '1', '1', 'article', '2', '2019-11-19 10:47:13', '2019-11-19 10:47:13');
INSERT INTO `mo_score_log` VALUES (21, '2', '1', 'article', '1', '2019-11-19 10:51:20', '2019-11-19 10:51:20');
INSERT INTO `mo_score_log` VALUES (22, '1', '1', 'article', '2', '2019-11-20 13:33:42', '2019-11-20 13:33:42');
INSERT INTO `mo_score_log` VALUES (23, '1', '1', 'article', '3', '2019-11-21 09:13:38', '2019-11-21 09:13:38');
INSERT INTO `mo_score_log` VALUES (24, '1', '1', 'article', '2', '2019-11-21 09:23:53', '2019-11-21 09:23:53');
INSERT INTO `mo_score_log` VALUES (25, '1', '1', 'article', '2', '2019-11-21 09:25:18', '2019-11-21 09:25:18');
INSERT INTO `mo_score_log` VALUES (26, '1', '1', 'article', '2', '2019-11-21 09:25:23', '2019-11-21 09:25:23');
INSERT INTO `mo_score_log` VALUES (27, '1', '1', 'article', '2', '2019-11-21 09:25:36', '2019-11-21 09:25:36');
INSERT INTO `mo_score_log` VALUES (28, '2', '1', 'article', '3', '2019-11-21 09:28:44', '2019-11-21 09:28:44');
INSERT INTO `mo_score_log` VALUES (29, '2', '1', 'article', '2', '2019-11-21 09:28:54', '2019-11-21 09:28:54');
INSERT INTO `mo_score_log` VALUES (30, '1', '1', 'article', '1', '2019-11-22 08:26:33', '2019-11-22 08:26:33');
INSERT INTO `mo_score_log` VALUES (31, '1', '1', 'article', '5', '2019-11-23 08:34:20', '2019-11-23 08:34:20');
INSERT INTO `mo_score_log` VALUES (32, '4', '1', 'article', '7', '2019-11-23 15:59:45', '2019-11-23 15:59:45');
INSERT INTO `mo_score_log` VALUES (33, '1', '1', 'article', '1', '2019-11-26 08:39:51', '2019-11-26 08:39:51');
INSERT INTO `mo_score_log` VALUES (34, '1', '1', 'article', '8', '2019-11-26 08:42:00', '2019-11-26 08:42:00');
INSERT INTO `mo_score_log` VALUES (35, '1', '1', 'article', '4', '2019-11-28 09:37:20', '2019-11-28 09:37:20');
INSERT INTO `mo_score_log` VALUES (36, '1', '1', 'article', '9', '2019-11-28 10:26:20', '2019-11-28 10:26:20');
INSERT INTO `mo_score_log` VALUES (37, '1', '1', 'article', '9', '2019-11-28 10:56:52', '2019-11-28 10:56:52');
INSERT INTO `mo_score_log` VALUES (38, '1', '1', 'article', '8', '2019-12-03 08:53:49', '2019-12-03 08:53:49');
INSERT INTO `mo_score_log` VALUES (39, '1', '1', 'article', '2', '2019-12-03 14:14:43', '2019-12-03 14:14:43');
INSERT INTO `mo_score_log` VALUES (40, '1', '1', 'article', '9', '2019-12-05 14:57:24', '2019-12-05 14:57:24');
INSERT INTO `mo_score_log` VALUES (41, '1', '1', 'article', '10', '2019-12-06 13:35:30', '2019-12-06 13:35:30');
INSERT INTO `mo_score_log` VALUES (42, '1', '1', 'article', '1', '2019-12-06 15:06:47', '2019-12-06 15:06:47');
INSERT INTO `mo_score_log` VALUES (43, '1', '1', 'article', '7', '2019-12-06 15:07:06', '2019-12-06 15:07:06');
INSERT INTO `mo_score_log` VALUES (44, '2', '1', 'article', '5', '2019-12-10 15:52:18', '2019-12-10 15:52:18');
INSERT INTO `mo_score_log` VALUES (45, '1', '1', 'article', '10', '2019-12-12 13:52:41', '2019-12-12 13:52:41');
INSERT INTO `mo_score_log` VALUES (46, '1', '1', 'article', '12', '2019-12-12 14:08:28', '2019-12-12 14:08:28');
INSERT INTO `mo_score_log` VALUES (47, '2', '1', 'article', '5', '2019-12-12 14:09:22', '2019-12-12 14:09:22');
INSERT INTO `mo_score_log` VALUES (48, '2', '1', 'article', '10', '2019-12-12 14:09:32', '2019-12-12 14:09:32');
INSERT INTO `mo_score_log` VALUES (49, '24', '1', 'article', '10', '2019-12-13 09:16:34', '2019-12-13 09:16:34');
INSERT INTO `mo_score_log` VALUES (50, '1', '1', 'article', '11', '2019-12-13 09:20:25', '2019-12-13 09:20:25');

-- ----------------------------
-- Table structure for mo_user
-- ----------------------------
DROP TABLE IF EXISTS `mo_user`;
CREATE TABLE `mo_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `score` int(255) NOT NULL,
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mo_user
-- ----------------------------
INSERT INTO `mo_user` VALUES (1, 'gyc', '别动我结菜', 'https://pubmosa.oss-cn-hangzhou.aliyuncs.com/user_logo.png', 103, '123456', '0', '2019-11-12 09:34:22', '2019-12-13 09:20:25');
INSERT INTO `mo_user` VALUES (2, 'gep', '郭智障', 'https://pubmosa.oss-cn-hangzhou.aliyuncs.com/qinlian.jpg', 7, '123456', '0', '2019-11-12 09:36:00', '2019-12-12 14:09:32');
INSERT INTO `mo_user` VALUES (3, 'paa', '无名用户', 'https://pubmosa.oss-cn-hangzhou.aliyuncs.com/logo.png', 0, '111111', '0', '2019-11-18 09:31:29', '2019-11-18 09:31:29');
INSERT INTO `mo_user` VALUES (24, '123', '无名用户', 'https://pubmosa.oss-cn-hangzhou.aliyuncs.com/h5.png', 1, '123', '0', '2019-12-13 09:15:41', '2019-12-13 09:16:34');

SET FOREIGN_KEY_CHECKS = 1;
