/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-09 21:37:49
 * @LastEditTime: 2019-10-06 15:09:12
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [{ //首页
            path: '/anli',
            name: 'Anli',
            component: resolve => require(['../components/mo/anli/anli.vue'], resolve)
        },
        { //安利
            path: '/',
            name: 'Index',
            component: resolve => require(['../components/mo/index/index.vue'], resolve)
        }, { //字体动画
            path: '/anime',
            name: 'Anime',
            component: resolve => require(['../components/mo/anime/animeText/animeText.vue'], resolve)
        }, { //动画测试
            path: '/animet',
            name: 'Animet',
            component: resolve => require(['../components/mo/anime/axiosLoading/axiosLoading3.vue'], resolve)
        }, { //组件测试
            path: '/test',
            name: 'Test',
            component: resolve => require(['../components/mo/alert/loadBox/loadBox.vue'], resolve)
        }, { //login    
            path: '/login',
            name: 'Login',
            component: resolve => require(['../components/mo/login/login.vue'], resolve)
        }, { //错误页面    
            path: '*',
            name: '404',
            component: resolve => require(['../components/mo/anime/select/select.vue'], resolve)
        }, { //文章    
            path: '/article',
            name: 'Article',
            component: resolve => require(['../components/mo/article/article.vue'], resolve)
        }, { //音乐    
            path: '/music',
            name: 'Music',
            component: resolve => require(['../components/mo/music/music.vue'], resolve)
        }, { //我的投稿    
            path: '/write',
            name: 'Write',
            component: resolve => require(['../components/mo/article/writeArticle/writeArticle.vue'], resolve),
            meta: {
                requireAuth: true
            }
        }, { //名人堂   
            path: '/honer',
            name: 'Honer',
            component: resolve => require(['../components/mo/honor/honor.vue'], resolve),
            meta: {
                requireAuth: true
            }
        }, { //播放模块   
            path: '/player',
            name: 'Player',
            component: resolve => require(['../components/mo/player/player.vue'], resolve)
        }, { //阅读文章   
            path: '/read',
            name: 'Read',
            component: resolve => require(['../components/mo/article/readArticle/readArticle.vue'], resolve)
        }, { //个人资料
            path: '/userinfo',
            name: 'Userinfo',
            component: resolve => require(['../components/mo/userinfo/userinfo.vue'], resolve),
            redirect: '/userinfoJb',
            children: [{
                path: '/userinfoGz',
                name: 'UserinfoGz',
                component: resolve => require(['../components/mo/userinfo/userinfoGz/userinfoGz.vue'], resolve),
            }, {
                path: '/userinfoSc',
                name: 'UserinfoSc',
                component: resolve => require(['../components/mo/userinfo/userinfoSc/userinfoSc.vue'], resolve),
            }, {
                path: '/userinfoJb',
                name: 'UserinfoJb',
                component: resolve => require(['../components/mo/userinfo/userinfoJb/userinfoJb.vue'], resolve),
            }, {
                path: '/userinfoFs',
                name: 'UserinfoFs',
                component: resolve => require(['../components/mo/userinfo/userinfoFs/userinfoFs.vue'], resolve),
            }]
        }, { //贡献榜
            path: '/contribute',
            name: 'Contribute',
            component: resolve => require(['../components/mo/contribute/contribute.vue'], resolve),
            meta: {
                requireAuth: true
            }
        }
    ]
})

// 判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
    sessionStorage.setItem('route', to.path)
    if (to.matched.some(res => res.meta.requireAuth)) { // 判断是否需要登录权限
        if (localStorage.getItem("loginfo1") == 'true') { // 判断是否登录
            next()
        } else { // 没登录则跳转到登录界面
            next({
                path: '/login',
                query: {
                    redirect: to.fullPath
                }
            })
        }
    } else {
        next()
    }
})

export default router