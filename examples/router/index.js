import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
    // mode:'history',
    routes: [
        { //折叠板
            path: '/fold',
            name: 'Fold',
            component: resolve => require(['../components/saoc/foldBox/foldbox'], resolve)
        },
        { //安利墙
            path: '/share',
            name: 'Share',
            component: resolve => require(['../components/saoc/shareWall/sharewall'], resolve)
        },
        { //画布
            path: '/canvas',
            name: 'Canvas',
            component: resolve => require(['../components/saoc/canvasBox/canvasbox'], resolve)
        },
        { //画布测试
            path: '/canvasT',
            name: 'CanvasT',
            component: resolve => require(['../components/saoc/canvasTest/canvastest'], resolve)
        },
        { //用户信息
            path: '/user',
            name: 'User',
            component: resolve => require(['../components/saoc/userInfo/userinfo'], resolve)
        },
        { //首页
            path: '/index',
            name: 'Index',
            component: resolve => require(['../components/saoc/indexBox2/index'], resolve)
        },
        { //输入框
            path: '/input',
            name: 'Input',
            component: resolve => require(['../components/saoc/inputBox/inputbox'], resolve)
        },
        { //轮播
            path: '/carousel',
            name: 'Carousel',
            component: resolve => require(['../components/saoc/carouselFigure/carouselfigure'], resolve)
        },
        { //动画
            path: '/animate',
            name: 'Animate',
            component: resolve => require(['../components/saoc/animateBox/animatebox'], resolve)
        },
        { //动画测试
            path: '/animateT',
            name: 'AnimateT',
            component: resolve => require(['../components/saoc/animateTest/animatetest'], resolve)
        },//排行榜 
        {
            path: '/ranking',
            name: 'Ranking',
            component: resolve => require(['../components/saoc/rankingList/rankinglist'], resolve)
        },//展示廊
        {
            path: '/showparty',
            name: 'Showparty',
            component: resolve => require(['../components/saoc/showParty/showparty'], resolve)
        },//y音乐盒子
        {
            path: '/musicbox',
            name: 'Musicbox',
            component: resolve => require(['../components/saoc/musicBox/musicbox'], resolve)
        }
    ]
})