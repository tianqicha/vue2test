/*
 * @Author: gyc
 * @Date: 2020-01-09 15:00:50
 * @LastEditTime : 2020-01-09 15:26:33
 * @Description: 测试版
 */

import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: []
})

// 判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
    sessionStorage.setItem('route', to.path)
    if (to.matched.some(res => res.meta.requireAuth)) { // 判断是否需要登录权限
        if (localStorage.getItem("loginfo1") == 'true') { // 判断是否登录
            next()
        } else { // 没登录则跳转到登录界面
            next({
                path: '/login',
                query: {
                    redirect: to.fullPath
                }
            })
        }
    } else {
        next()
    }
})

export default router