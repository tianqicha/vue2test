import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

export default new Router({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: [{ //动画测试
        path: '/',
        name: 'main',
        component: resolve => require(['../../components/mo/anime/loading/loading.vue'], resolve)
    }],
});
