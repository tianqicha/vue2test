/*
 * @Description: In User Settings Edit
 * @Author: gyc
 * @Date: 2019-09-12 21:35:46
 * @LastEditTime: 2019-10-02 12:47:26
 * @LastEditors: Please set LastEditors
 */
import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)

const user = {
    state: { //状态
        Count: 0,//在线人数
        isLogin: false,//登录
        logined: false,//登录状态
        myurl: 'http://47.94.244.199:3000/api/v1',
        testurl: 'http://localhost:3000/api/v1',
        ossUrl: 'http://47.94.244.199:3000/oss/v1',
        testOssUrl: 'http://localhost:3000/oss/v1',
    },
    computed: { //计算返回新属性

    },
    getters: { //获取属性
        count(state) {
            return state.Count;
        },
        login(state) {
            return state.isLogin
        },
        logins(state) {
            return state.logined
        },
        url(state) {
            return state.testurl
            // return state.myurl
        },
        ossurl(state) {
            return state.testOssUrl
            // return state.ossUrl
        }
    },
    mutations: { //改变state
        goLogin(state) {
            state.isLogin = false;
        },
        noLogin(state) {
            state.isLogin = true;
        },
        nowLogins(state) {
            state.logined = true;
        }
    },
    actions: { //触发mutations
        goLogin(context) {
            context.commit("goLogin");
        },
        noLogin(context) {
            context.commit("noLogin");
        },
        nowLogins(context) {
            context.commit("nowLogins");
        }
    }
}

export const store = new Vuex.Store({
    modules: {
        user: user
    }
})