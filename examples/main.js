/*
 * @Author: gyc
 * @Date: 2019-10-30 09:06:35
 * @LastEditTime : 2020-01-13 15:16:33
 * @Description: 测试版
 */

import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
// import '@babel/polyfill'
import 'lib-flexible/flexible'
import 'view-design/dist/styles/iview.css';
// import Blob from '../excel/Blob.js'
// import Export2Excel from '../excel/Export2Excel.js'
import axios from 'axios'
import moment from 'moment'
import '../my-theme/index.less';
import VueParticles from 'vue-particles'

import {
  store
} from './store/store'
import {
  Tooltip,
  Icon,
  Message
} from 'view-design';
Vue.config.productionTip = false
Vue.component('Tooltip', Tooltip)
Vue.component('Icon', Icon)
axios.defaults.withCredentials = true //跨域
Vue.prototype.$Message = Message
Vue.prototype.$axios = axios
Vue.prototype.$moment = moment
moment.locale('zh-cn')
// Vue.prototype.$baseUrl = process.env.BASE_URL
Vue.use(VueParticles)

// 超时时间
axios.defaults.timeout = 5000
// http请求拦截器
let message
axios.interceptors.request.use(config => {
  // message=Message.loading({
  //   content: 'Loading...',
  //   duration: 0
  // });
  return config
}, error => {
  loadinginstace.close()
  Message.error({
    message: '加载超时'
  })
  return Promise.reject(error)
})

// http响应拦截器
axios.interceptors.response.use(data => { // 响应成功关闭loading
  //   Message.success({
  //   content: '數據加載完成！',
  //   duration: 3
  // });
  // setTimeout(message, 0);
  return data
}, error => {
  Message.error({
    content: '加载失败！'
  })
  return Promise.reject(error)
})

new Vue({
  store: store, //使用store
  render: h => h(App),
  router,
  mounted() {
    document.dispatchEvent(new Event('render-event'))
  }
}).$mount('#app')