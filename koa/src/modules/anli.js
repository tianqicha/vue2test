/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:52:50
 * @LastEditTime: 2019-09-17 17:45:44
 * @LastEditors: Please set LastEditors
 */
// 引入mysql的配置文件
const db = require('../config/db');

// 引入sequelize对象
const Sequelize = db.sequelize;

// 引入数据表模型
const AnliDong = Sequelize.import('../schema/anli_dongdan.js');
const AnliArea = Sequelize.import('../schema/anli_area.js');
const AnliAreaLove = Sequelize.import('../schema/anli_area_love.js');
const User = Sequelize.import('../schema/user.js');

AnliDong.sync({
    force: false,
    logging: false
}); //自动创建表

AnliArea.sync({
    force: false,
    logging: false
}); //自动创建表

AnliAreaLove.sync({
    force: false,
    logging: false
}); //自动创建表

AnliDong.belongsTo(User, {
    as: 'userinfo',
    foreignKey: 'userId',
    targetKey: 'id'
});

AnliArea.belongsTo(AnliAreaLove, {
    as: 'user',
    foreignKey: 'id',
    targetKey: 'anli_Id'
});

class AnliModel {
    /**
     * 创建动弹模型
     * @param data
     * @returns {Promise<*>}
     */
    static async createAnliDongdan(data) {
        return await AnliDong.create({
            userId: data.userId,
            dongForm: data.dongForm,
            love: 0,
            isLock: 0
        });
    }

    /**
     * 查询所有动弹
     * @param 
     * @returns {Promise<Model>}
     */
    static async getDongdan() {
        return await AnliDong.findAll({
            limit: 10,
            include: [{ // include关键字表示关联查询
                model: User, // 指定关联的model
                as: 'userinfo', // 由于前面建立映射关系时为class表起了别名，那么这里也要与前面保持一致，否则会报错
                attributes: ['nickname','avatar'],
            }],
            order: [
                ['createdAt', 'DESC']
            ]
        });
    }

        /**
     * 查询我的最新动弹
     * @param 
     * @returns {Promise<Model>}
     */
    static async getOwnNewDongdan(userId) {
        return await AnliDong.findOne({
            // limit: 5,
            where:{
                userId:userId
            },
            order: [
                ['createdAt', 'DESC']
            ]
        });
    }

    /**
     * 查询所有安利
     * @param 
     * @returns {Promise<Model>}
     */
    static async getAnli(userId) {
        return await AnliArea.findAll({
            where: {
                isLock: 0
            },
            include: [{ // include关键字表示关联查询
                model: AnliAreaLove, // 指定关联的model
                as: 'user', // 由于前面建立映射关系时为class表起了别名，那么这里也要与前面保持一致，否则会报错
                attributes: ['userId', 'isDel'],
                required: false,
                where: {
                    userId: userId
                }
            }],
            limit:50
        });
    }

    /**
     * 创建我的喜爱安利
     * @param 
     * @returns {Promise<Model>}
     */
    static async loveAnli(data) {
        return await AnliAreaLove.create({
            anli_Id: data.anli_Id,
            userId: data.userId,
            isDel: 0
        });
    }

    /**
     * 查询我的喜爱安利
     * @param 
     * @returns {Promise<Model>}
     */
    static async searchLoveAnli(data) {
        return await AnliAreaLove.findOne({
            where: {
                anli_Id: data.anli_Id,
                userId: data.userId
            }
        });
    }
}


module.exports = AnliModel;