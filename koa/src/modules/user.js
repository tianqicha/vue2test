/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:52:50
 * @LastEditTime: 2019-09-17 17:45:44
 * @LastEditors: Please set LastEditors
 */
// 引入mysql的配置文件
const db = require('../config/db');

// 引入sequelize对象
const Sequelize = db.sequelize;

// 引入数据表模型
const User = Sequelize.import('../schema/user');
const Userinfo = Sequelize.import('../schema/userinfo');
User.sync({
    force: false,
    logging: false
}); //自动创建表

Userinfo.sync({
    force: false,
    logging: false
}); //自动创建表

class UserModel {
    /**
     * 创建用户模型
     * @param data
     * @returns {Promise<*>}
     */
    static async createUser(data) {
        return await User.create({
            user: data.user,
            pwd: data.pwd,
            lock: '1',
            nickname: "无名用户",
            score: 0,
            avatar: 'https://pubmosa.oss-cn-hangzhou.aliyuncs.com/avatar/' + data.avatar
        });
    }


    /**
     * 创建用户信息模型
     * @param data
     * @returns {Promise<*>}
     */
    static async createUserinfo(data) {
        return await Userinfo.create({
            userid: data.userid,
            area: data.area,
            old: data.old,
            email: data.email,
            constellation: data.constellation,
            major: data.major,
            hobby: data.hobby,
            love: data.love,
            shokai: data.shokai,
            shejiao: ""
        });
    }

    /**
     * 查询用户信息（userinfo）
     * @param data
     * @returns {Promise<*>}
     */
    static async getUserinfoo(userid) {
        return await Userinfo.findOne({
            // attributes: ['id'],
            where: {
                userid: userid,
            }
        });
    }

    /**
     * 更新用户信息（userinfo）
     * @param data
     * @returns {Promise<*>}
     */
    static async updateUserinfoo(data) {
        return await Userinfo.update({
            area: data.area,
            old: data.old,
            email: data.email,
            constellation: data.constellation,
            major: data.major,
            hobby: data.hobby,
            love: data.love,
            shokai: data.shokai,
            shejiao: ""
        }, {
            where: {
                userid: data.userid
            }
        })
    }


    /**
     * 查询用户名
     * @param user
     * @returns {Promise<Model>}
     */
    static async getUserName(user) {
        return await User.findOne({
            where: {
                user
            }
        });
    }

    /**
     * 查询用户信息
     * @param id
     * @returns {Promise<Model>}
     */
    static async getUserInfo(id) {
        return await User.findOne({
            where: {
                id
            }
        });
    }

    /**
     * 用户登录
     * @param user,pwd
     * @returns login
     */
    static async login(user, pwd) {
        return await User.findOne({
            where: {
                user,
                pwd,
            }
        });
    }
}


module.exports = UserModel;