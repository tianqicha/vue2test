// 引入mysql的配置文件
const db = require('../config/db');

// 引入sequelize对象
const Sequelize = db.sequelize;



// 引入数据表模型
const Review = Sequelize.import('../schema/review');
const User = Sequelize.import('../schema/user');
// const Score = Sequelize.import('../schema/score');


Review.belongsTo(User, {
    as: 'user',
    foreignKey: 'ownerId',
    targetKey: 'id'
});


Review.sync({
    force: false,
    logging: false
}); //自动创建表

// Score.sync({
//     force: false,
//     logging: false
// }); //自动创建表

class ReviewModel {

    /**
     * 创建评论模型(无积分)
     * @param data
     * @returns {Promise<*>}
     */

    static async createReview(data) {
        return await Review.create({
            review: data.review,
            ownerId: data.ownerId,
            lock: '0',
            zan: 0,
            reviewTo: data.reviewTo,
            articleId: data.articleId
        })
    }

    /**
     * 创建评论模型(有效积分)
     * @param data
     * @returns {Promise<*>}
     */
    static async createReviewScore(data) {

        return Sequelize.transaction(function (t) {

            // 在这里链接您的所有查询。 确保你返回他们。
            return Review.create({
                review: data.review,
                ownerId: data.ownerId,
                lock: '0',
                zan: 0,
                reviewTo: data.reviewTo,
                articleId: data.articleId
            }, {
                transaction: t
            }).then(function (data) {
                return User.update({
                    score: Sequelize.literal("score+1")
                }, {
                    where: {
                        id: data.ownerId
                    },
                    fields: ['score']
                }, {
                    transaction: t
                })
            });

        }).then(function (result) {
            // 事务已被提交
            // result 是 promise 链返回到事务回调的结果
            return result;
        }).catch(function (err) {
            // 事务已被回滚
            // err 是拒绝 promise 链返回到事务回调的错误
        });
    }


    /**
     * 查询评论
     * @param articleid
     * @returns {Promise<Model>}
     */
    static async getReview(articleid) {
        return await Review.findAll({
            where: {
                articleid: articleid
            },
            // limit: 5,
            // offset: page * 5,
            include: [{
                model: User,
                as: 'user',
                attributes: ['nickname', 'avatar']
            }],
            order: [
                ['createdAt', 'DESC']
            ]
        });
    }


    /**
     * 查询本人所有评论
     * @param ownerId
     * @returns {Promise<Model>}
     */
    static async getOwnerReview(ownerId) {
        return await Review.findAll({
            where: {
                ownerId
            },
            order: [
                ['createdAt', 'DESC']
            ]
        });
    }

    /**
     * 查询本人最新评论
     * @param ownerId
     * @returns {Promise<Model>}
     */
    static async getOwnerReview2(ownerId, articleId) {
        return await Review.findOne({
            where: {
                ownerId,
                articleId
            },
            include: [{
                model: User,
                as: 'user',
                attributes: ['nickname', 'avatar']
            }],
            order: [
                ['createdAt', 'DESC']
            ]
        });
    }

    /**
     * 删除本人评论
     * @param reviewId
     * @returns {Promise<Model>}
     */
    static async destroyReview(reviewId) {
        return await Review.destroy({
            where: {
                id: reviewId
            }
        });
    }

    /**
     * @description: 全部评论数目
     * @param articleId 
     * @return: reviewCount
     */
    static async reviewCount(articleId) {
        return await Review.count({
            where: {
                articleId: articleId,
                lock: '0'
            },
        })
    }

    /**
     * @description: 本人评论数目
     * @param ownerId 
     * @return: reviewCount
     */
    static async ownerReviewCount(ownerId) {
        return await Review.count({
            where: {
                ownerId,
                createdAt: {
                    $lt: new Date(),
                    $gt: (new Date() - 24 * 60 * 60 * 1000)
                }
            },
        })
    }
}

module.exports = ReviewModel