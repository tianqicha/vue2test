/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:52:50
 * @LastEditTime: 2019-09-17 17:45:44
 * @LastEditors: Please set LastEditors
 */
// 引入mysql的配置文件
const db = require('../config/db');

// 引入sequelize对象
const Sequelize = db.sequelize;

const date=new Date();

const moment = require('moment')

// 引入数据表模型
const Score = Sequelize.import('../schema/score');
Score.sync({
    force: false,
    logging: false
}); //自动创建表

class ScoreModel {
    /**
     * 创建分数日志模型
     * @param data
     * @returns {Promise<*>}
     */
    static async createScoreLog(data) {
        return await Score.create({
            userId: data.userId,
            letter: data.letter,
            score: data.score,
            scoreFrom: data.scoreFrom
        });
    }


    /**
     * 查询用户发言数（今日）
     * @param id
     * @returns {Promise<Model>}
     */
    static async getOwnerScopeCount(ownerId) {
        return await Score.count({
            where: {
                userId:ownerId,
                createdAt: {
                    $lt: moment().add(1, 'days').format('YYYY-MM-DD'),
                    $gt: moment().format('YYYY-MM-DD')
                }
            },
        });
    }
}


module.exports = ScoreModel;