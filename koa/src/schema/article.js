/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-15 12:02:51
 * @LastEditTime: 2019-09-16 18:55:30
 * @LastEditors: Please set LastEditors
 */
const moment = require("moment");
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('mo_article', {
        id: {
            type: DataTypes.INTEGER(255),
            primaryKey: true,
            allowNull: true,
            autoIncrement: true
        },
        article: {
            type: DataTypes.TEXT,
            allowNull: false,
            field: 'article'
        },
        owner: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'owner'
        },
        ishow: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'ishow',
        },
        islock: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'islock',
        },
        iclass: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'iclass',
        },
        imoshi: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'imoshi',
        },
        ijianjie: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'ijianjie',
        },
        igaiyao: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'igaiyao',
        },
        ititle: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'ititle',
        },
        isee: {
            type: DataTypes.INTEGER(255),
            allowNull: false,
            field: 'isee',
        },
        artid: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'artid',
        },
        // 创建时间
        createdAt: {
            type: DataTypes.DATE
        },
        // 更新时间
        updatedAt: {
            type: DataTypes.DATE
        }
    }
        , {
            /**
             * 如果为true，则表示名称和model相同，即user
             * 如果为fasle，mysql创建的表名称会是复数，即users
             * 如果指定的表名称本身就是复数，则形式不变
             */
            freezeTableName: true
        });
}