/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:53:00
 * @LastEditTime: 2019-09-15 13:00:08
 * @LastEditors: Please set LastEditors
 */
const moment = require("moment");
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('mo_anli_area', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: true,
            autoIncrement: true
        },
        //安利名称
        anli_Name: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'anli_Name'
        },
        //制作人
        anli_Author: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'anli_Author'
        },
        //安利简介
        anli_Jianjie: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'anli_Jianjie'
        },
        //安利链接
        anli_Url: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'anli_Url'
        },
        //安利链接
        anli_imgUrl: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'anli_imgUrl'
        },
        //安利链接
        isLock: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'isLock'
        },
        //安利热度
        hot: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'hot'
        },
        // 创建时间
        createdAt: {
            type: DataTypes.DATE
        },
        // 更新时间
        updatedAt: {
            type: DataTypes.DATE
        }
    }, {
        /**
         * 如果为true，则表示名称和model相同，即user
         * 如果为fasle，mysql创建的表名称会是复数，即users
         * 如果指定的表名称本身就是复数，则形式不变
         */
        freezeTableName: true
    });
}