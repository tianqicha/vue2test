/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:53:00
 * @LastEditTime: 2019-09-15 13:00:08
 * @LastEditors: Please set LastEditors
 */
const moment = require("moment");
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('mo_userinfo', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: true,
            autoIncrement: true
        },
        //用户id
        userid: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'userid'
        },
        //归属地区
        area: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'area'
        },
        //年龄
        old: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'old'
        },
        //邮箱
        email: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'email'
        },
        //星座
        constellation: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'constellation'
        },
        //专业
        major: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'major',
        },
        //爱好
        hobby: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'hobby',
        },
        //喜欢
        love: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'love',
        },
        //介绍
        shokai: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'shokai',
        },
        //社交账号
        shejiao: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'shejiao',
        },
        // 创建时间
        createdAt: {
            type: DataTypes.DATE
        },
        // 更新时间
        updatedAt: {
            type: DataTypes.DATE
        }
    }, {
        /**
         * 如果为true，则表示名称和model相同，即user
         * 如果为fasle，mysql创建的表名称会是复数，即users
         * 如果指定的表名称本身就是复数，则形式不变
         */
        freezeTableName: true
    });
}