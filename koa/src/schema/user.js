/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:53:00
 * @LastEditTime: 2019-09-15 13:00:08
 * @LastEditors: Please set LastEditors
 */
const moment = require("moment");
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('mo_user', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: true,
            autoIncrement: true
        },
        //用户名
        user: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'user'
        },
        //昵称
        nickname: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'nickname'
        },
        //头像
        avatar: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'avatar'
        },
        //密码
        pwd: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'pwd'
        },
        //锁
        lock: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'lock',
        },
        //积分
        score: {
            type: DataTypes.INTEGER(255),
            allowNull: false,
            field: 'score',
        },
        // 创建时间
        createdAt: {
            type: DataTypes.DATE
        },
        // 更新时间
        updatedAt: {
            type: DataTypes.DATE
        }
    }, {
        /**
         * 如果为true，则表示名称和model相同，即user
         * 如果为fasle，mysql创建的表名称会是复数，即users
         * 如果指定的表名称本身就是复数，则形式不变
         */
        freezeTableName: true
    });
}