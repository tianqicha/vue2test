var koa = require('koa');
var model = require('./config/model');
var oauthserver = require('oauth2-server');
var bodyParser = require('body-parser')

var app =new koa();
app.use(bodyParser)

app.oauth = oauthserver({
  model: model, // 查看https://github.com/thomseddon/node-oauth2-server for specification
  grants: ['password'],
  debug: true
});

app.use(app.oauth.authorise());

app.use(async (ctx) => {
  ctx.body = 'Secret area';
});

// console.log(app.oauth)
app.use(app.oauth.errorHandler());
console.log('start at 3003 point!')
app.listen(3003);