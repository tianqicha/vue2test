const Koa = require('koa')
const app = new Koa()
const path = require('path')
const history = require('koa-connect-history-api-fallback')
const static = require('koa-static')
const compress = require('koa-compress');
const options = { threshold: 2048 };
const staticPath = './../../dist';

app.use(compress(options));
app.use(history());
app.use(static(
    path.join(__dirname, staticPath)
))
app.listen(3001)
console.log('[demo] start-quick is starting at port 3001')