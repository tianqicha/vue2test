/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-09 21:37:49
 * @LastEditTime: 2019-09-09 21:37:49
 * @LastEditors: your name
 */
const mysql = require('mysql')
const config = require('./db2')
//连接数据库
const pool = mysql.createPool({
    host: config.database.HOST,
    user: config.database.USERNAME,
    password: config.database.PASSWORD,
    database: config.database.DATABASE,
    port: config.database.PORT,
    multipleStatements: true//允许多条sql同时执行s
});
//sql执行
let query = function (sql, values) {
    return new Promise((resolve, reject) => {
        pool.getConnection(function (err, connection) {
            if (err) {
                reject(err)
            } else {
                connection.query(sql, values, (err, rows) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(rows)
                    }
                    connection.release()
                })
            }
        })
    })
}
module.exports = query
