/*
 * @Author: gyc
 * @Date: 2019-11-25 14:41:55
 * @LastEditTime: 2020-02-21 14:01:04
 * @Description: 测试版
 */
var model = module.exports,
    util = require('util'),
    redis = require('redis');

var db = redis.createClient(6379, '47.94.244.199', {
    db: 1,
});

db.multi()
    .hmset('users:username', {
        id: '1',
        username: 'gyc',
        password: '123456'
    })
    .hmset('clients:client', {
        clientId: 'client',
        clientSecret: 'secret'
    })
    .sadd('clients:client:grant_types', [
        'password',
        'refresh_token'
    ])
    .exec(function (errs) {
        if (errs) {
            console.error(errs[0].message);
            return process.exit(1);
        }

        console.log('Client and user added successfully');
        process.exit();
    });

var keys = {
    token: 'tokens:%s',
    client: 'clients:%s',
    refreshToken: 'refresh_tokens:%s',
    grantTypes: 'clients:%s:grant_types',
    user: 'users:%s'
};

model.getAccessToken = function (bearerToken, callback) {
    db.hgetall(util.format(keys.token, bearerToken), function (err, token) {
        if (err) return callback(err);

        if (!token) return callback();

        callback(null, {
            accessToken: token.accessToken,
            clientId: token.clientId,
            expires: token.expires ? new Date(token.expires) : null,
            userId: token.userId
        });
    });
};

model.getClient = function (clientId, clientSecret, callback) {
    db.hgetall(util.format(keys.client, clientId), function (err, client) {
        if (err) return callback(err);

        if (!client || client.clientSecret !== clientSecret) return callback();

        callback(null, {
            clientId: client.clientId,
            clientSecret: client.clientSecret
        });
    });
};

model.getRefreshToken = function (bearerToken, callback) {
    db.hgetall(util.format(keys.refreshToken, bearerToken), function (err, token) {
        if (err) return callback(err);

        if (!token) return callback();

        callback(null, {
            refreshToken: token.accessToken,
            clientId: token.clientId,
            expires: token.expires ? new Date(token.expires) : null,
            userId: token.userId
        });
    });
};

model.grantTypeAllowed = function (clientId, grantType, callback) {
    db.sismember(util.format(keys.grantTypes, clientId), grantType, callback);
};

model.saveAccessToken = function (accessToken, clientId, expires, user, callback) {
    db.hmset(util.format(keys.token, accessToken), {
        accessToken: accessToken,
        clientId: clientId,
        expires: expires ? expires.toISOString() : null,
        userId: user.id
    }, callback);
};

model.saveRefreshToken = function (refreshToken, clientId, expires, user, callback) {
    db.hmset(util.format(keys.refreshToken, refreshToken), {
        refreshToken: refreshToken,
        clientId: clientId,
        expires: expires ? expires.toISOString() : null,
        userId: user.id
    }, callback);
};

model.getUser = function (username, password, callback) {
    db.hgetall(util.format(keys.user, username), function (err, user) {
        if (err) return callback(err);

        if (!user || password !== user.password) return callback();

        callback(null, {
            id: username
        });
    });
};