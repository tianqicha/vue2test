const ReviewModel = require("../modules/review");
const ScoreModel = require("../modules/score");
const moment = require("moment");
class ReviewController {
    /**
     * 评论创建
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async create(ctx) {
        //接收客服端
        let req = ctx.request.query;
        let nextTime
        if (!!req) {
            async function getNextTime() {
                let isLock = await ReviewModel.getOwnerReview(req.ownerId);
                if (isLock.length != 0) {
                    nextTime = (moment(new Date()).valueOf() - moment(isLock[0].createdAt).valueOf()) / 1000
                } else {
                    nextTime = 10
                }
            }
            await getNextTime();
            if (nextTime >= 10) {
                try {
                    //判断是否达到今日积分上限
                    let result = await ScoreModel.getOwnerScopeCount(req.ownerId);
                    if (result < 5) {
                        await ReviewModel.createReviewScore(req);
                        let result2 = await ReviewModel.getOwnerReview2(req.ownerId, req.articleId);
                        await ScoreModel.createScoreLog({//写入积分日志表
                            scoreFrom: 'article',
                            letter: req.articleId,
                            score: '1',
                            userId: req.ownerId
                        })
                        ctx.response.status = 200;
                        ctx.body = {
                            code: 200,
                            msg: '创建评论成功,积分加1',
                            result2: result2
                        }
                    } else {
                        await ReviewModel.createReview(req);
                        let result2 = await ReviewModel.getOwnerReview2(req.ownerId, req.articleId);
                        ctx.response.status = 200;
                        ctx.body = {
                            code: 200,
                            msg: '创建评论成功，不加积分',
                            result2: result2
                        }
                    }

                } catch (err) {
                    ctx.response.status = 412;
                    ctx.body = {
                        code: 412,
                        msg: '创建评论失败',
                        data: {
                            err: err
                        }
                    }
                }
            } else {
                console.log(nextTime)
                // ctx.response.status = 418;
                ctx.body = {
                    code: 418,
                    msg: '时间间隔不足10',
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * 查询评论
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async getReview(ctx) {
        //接收客服端
        let req = ctx.request.query;
        if (!!req) {
            try {
                let result = await ReviewModel.getReview(req.articleId);
                let result2 = await ReviewModel.reviewCount(req.articleId);
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询评论成功',
                    result,
                    result2
                }
            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '查询评论失败',
                    data: {
                        err: err
                    }
                }
            }
        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * 删除评论
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async destroyReview(ctx) {
        //接收客服端
        let req = ctx.request.query;
        if (!!req) {
            try {
                let result = await ReviewModel.destroyReview(req.id);
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '删除评论成功',
                    result,
                }

            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '删除评论失败',
                    data: {
                        err: err
                    }
                }
            }
        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }
}

module.exports = ReviewController