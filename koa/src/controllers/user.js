/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:52:40
 * @LastEditTime: 2019-09-17 17:46:11
 * @LastEditors: Please set LastEditors
 */

const UserModel = require("../modules/user");
const encrypt = require("../config/encrypt")
class userController {
    /**
     * 用户创建
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async create(ctx) {
        //接收客服端
        let req = ctx.request.query;
        console.log(req)
        if (req.user && req.pwd) {
            try {
                const data = await UserModel.getUserName(req.user);
                if (!!(data)) {
                    ctx.response.status = 201;
                    ctx.body = {
                        code: 201,
                        msg: '用户已存在',
                    }
                } else {
                    let result = await UserModel.createUser(req);
                    ctx.response.status = 200;
                    ctx.body = {
                        code: 200,
                        msg: '创建用户成功',
                        result
                    }
                }

            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '创建用户失败',
                    data: {
                        err: err
                    }
                }
            }
        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 200,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * 测试用户
     * @param ctx
     * @returns testUser
     */

    static async testUser(ctx) {
        let user = ctx.request.query.user;
        if (user) {
            try {
                // 查询用户
                let data = await UserModel.getUserName(user);
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }
            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '查询失败',
                    data
                }
            }
        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: 'user必须传'
            }
        }
    }

    /**
     * 查询用户
     * @param ctx
     * @returns testUser
     */

    static async userinfo(ctx) {
        let id = ctx.request.query.id;
        if (id) {
            try {
                // 查询用户
                let data = await UserModel.getUserInfo(id);
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }
            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '查询失败',
                    data
                }
            }
        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: 'ID必须传'
            }
        }
    }

    /**
     * @description:登录 
     * @param ctx 
     * @return: login
     */
    static async login(ctx) {
        let key = "1314"
        let user = encrypt.Decrypt(ctx.request.query.user, key);
        let pwd = encrypt.Decrypt(ctx.request.query.pwd, key);
        if (user && pwd) {
            try {
                // 用户登录
                let data = await UserModel.login(user, pwd);
                if (!!data) {
                    if (data.lock == 0) {
                        ctx.session.userinfo = {
                            id: data.id,
                            lock: data.lock
                        }
                        // ctx.cookies.set('loginfo', {
                        //     id: data.id,
                        //     status: true
                        // }, {
                        //     maxAge: 3600 * 1000 * 1000
                        // })
                        ctx.response.status = 200;
                        ctx.body = {
                            code: 200,
                            msg: '登录成功',
                            data
                        }
                    } else {
                        ctx.response.status = 202;
                        ctx.body = {
                            code: 202,
                            msg: '账号被封停！',
                            data
                        }
                    }

                } else {
                    ctx.response.status = 201;
                    ctx.body = {
                        code: 201,
                        msg: '账号密码不正确',
                        data
                    }
                }

            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '查询失败',
                    data
                }
            }
        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '信息不全'
            }
        }
    }

    /**
     * 验证登录
     * @param {type} 
     * @return: {Promise.<void>}
     */
    static async nowlogin(ctx) {
        let userstates = ctx.session.userinfo
        console.log(userstates)
        ctx.body = userstates
    }


    /**
     * 创建用户信息
     * @param {type} 
     * @return: {Promise.<void>}
     */
    static async createUserinfo(ctx) {
        let data = ctx.request.query;
        if (data) {
            const result2 = await UserModel.getUserinfoo(data.userid)
            console.log(result2)
            if ((!!!result2)) {
                const result = await UserModel.createUserinfo(data)
                ctx.response.status = 200;
                ctx.body = {
                    msg: "创建成功！",
                    code: 200,
                    result,
                }
            } else {
                const result = await UserModel.updateUserinfoo(data)
                ctx.response.status = 200;
                ctx.body = {
                    msg: "更新成功！",
                    code: 200,
                    result
                }
            }

        }
    }

    /**
     * 查询用户信息
     * @param {type} 
     * @return: {Promise.<void>}
     */
    static async getUserinfoo(ctx) {
        let data = ctx.request.query;
        if (data) {
            const result = await UserModel.getUserinfoo(data.userid)
            ctx.response.status = 200;
            ctx.body = {
                msg: "更新成功！",
                code: 200,
                result
            }
        }
    }

}


module.exports = userController;