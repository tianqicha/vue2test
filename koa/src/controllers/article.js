/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-15 12:17:52
 * @LastEditTime: 2019-09-17 11:09:52
 * @LastEditors: Please set LastEditors
 */
const ArticleModel = require("../modules/article");
const encrypt = require("../config/encrypt")
class articleController {
    /**
     * 创建文章
     * @param ctx
     * @returns create
     */
    static async create(ctx) {
        const req = ctx.request.body.data;
        // console.log(req)
        if (req) {
            let count = await ArticleModel.myArticleCounts(req)
            console.log(count.length)
            try {
                if (count.length < 3) {
                    let data = await ArticleModel.createArticle(req)
                    ctx.response.status = 200;
                    ctx.body = {
                        code: 200,
                        msg: '提交成功',
                        data
                    }
                } else {
                    ctx.response.status = 201;
                    ctx.body = {
                        code: 201,
                        msg: '已存在三个未审核文章',
                    }
                }
            } catch (err) {
                ctx.response.status = 412;
                ctx.body = {
                    code: 412,
                    msg: '提交失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }


    }

    /**
     * @description: 阅读文章
     * @param ctx
     * @return: readArtcile
     */
    static async readArtcile(ctx) {
        let key = "1314"
        const req = ctx.request.query;
        const mid = encrypt.Decrypt(req.artid, key)
        if (req) {
            try {
                let data = await ArticleModel.readArtcile(req.artid)
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }
            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '查询失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }


    }

    /**
     * @description: 查询文章
     * @param {type} 
     * @return: 
     */
    static async artcileList(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let data = await ArticleModel.articleList(req.page, req.userId)
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }
            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '查询失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }


    }

    /**
     * @description: 文章数目
     * @param {type} 
     * @return: 
     */
    static async artcileCount(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let data = await ArticleModel.articleCount()
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }
            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '查询失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * @description: 浏览次数
     * @param {type} 
     * @return: 
     */
    static async artcileIsee(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let data = await ArticleModel.artcileIsee(req.id)
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }
            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '查询失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * @description: 添加收藏
     * @param {type} 
     * @return: 
     */
    static async articleLove(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let isLove = await ArticleModel.findLove(req)
                console.log(isLove)
                if (!!isLove === false) {
                    let data = await ArticleModel.articleLove(req)
                    ctx.response.status = 200;
                    ctx.body = {
                        code: 200,
                        msg: '添加成功',
                        data
                    }
                } else {
                    ctx.response.status = 202;
                    ctx.body = {
                        code: 202,
                        msg: '已收藏',
                        isLove
                    }
                }

            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '添加失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * @description: 查寻收藏
     * @param {type} 
     * @return: 
     */
    static async getMyArticle(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let data = await ArticleModel.getMyArticle(req)
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }

            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '查询失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * @description: 查寻收藏
     * @param {type} 
     * @return: 
     */
    static async getMyAllArticle(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let data = await ArticleModel.findMyAllLove(req)
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '查询成功',
                    data
                }

            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '查询失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * @description: 查寻收藏
     * @param {type} 
     * @return: 
     */
    static async deleteLove(ctx) {
        const req = ctx.request.query;
        if (req) {
            try {
                let data = await ArticleModel.deleteLove(req)
                ctx.response.status = 200;
                ctx.body = {
                    code: 200,
                    msg: '删除成功',
                    data
                }

            } catch (err) {
                ctx.response.status = 201;
                ctx.body = {
                    code: 201,
                    msg: '删除失败',
                    err
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * @description: 查询文章列表
     * @param {type} 
     * @return: 
     */
    static async getMyArticleList(ctx) {
        const req = ctx.request.query
        const data = await ArticleModel.getMyArticleList(req)
        ctx.response.status = 200;
        ctx.body = {
            code: 200,
            msg: "查询成功！",
            data
        }
    }

    /**
     * @description: 查询文章内容
     * @param {type} 
     * @return: 
     */
    static async getMyArticleMain(ctx) {
        const req = ctx.request.query
        const data = await ArticleModel.getMyArticleMain(req)
        ctx.response.status = 200;
        ctx.body = {
            code: 200,
            msg: "查询成功！",
            data
        }
    }

    /**
     * @description: 更新我的文章内容
     * @param {type} 
     * @return: 
     */
    static async updateMyArticle(ctx) {
        const req = ctx.request.body.data
        const data = await ArticleModel.updateMyArticle(req)
        try {
            ctx.response.status = 200;
            ctx.body = {
                code: 200,
                msg: "更新成功！",
                data
            }
        } catch (err) {
            ctx.response.status = 500;
            ctx.body = {
                code: 500,
                msg: "更新失败！",
                err
            }
        }



    }
}

module.exports = articleController;