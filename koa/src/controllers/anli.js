/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-14 17:52:40
 * @LastEditTime: 2019-09-17 17:46:11
 * @LastEditors: Please set LastEditors
 */

const AnliModel = require("../modules/anli");
const UserModel = require("../modules/user");
const encrypt = require("../config/encrypt");
const moment = require("moment");
class anliController {
    /**
     * 动弹创建
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async create(ctx) {
        //接收客服端
        let req = ctx.request.query;
        if (req) {
            const limtT = await AnliModel.getOwnNewDongdan(req.userId)
            if (!!limtT) {
                const nextTime = (moment(new Date()).valueOf() - moment(limtT.createdAt).valueOf()) / 1000;
                if (nextTime > 20) {
                    try {
                        const data = await AnliModel.createAnliDongdan(req);
                        const user = await UserModel.getUserInfo(req.userId)
                        ctx.response.status = 200;
                        ctx.body = {
                            code: 200,
                            msg: '创建动弹成功',
                            data,
                            user
                        }

                    } catch (err) {
                        ctx.response.status = 412;
                        ctx.body = {
                            code: 412,
                            msg: '创建动弹失败',
                            data: {
                                err: err
                            }
                        }
                    }
                } else {
                    ctx.response.status = 201;
                    ctx.body = {
                        code: 201,
                        msg: '发言时间太短',
                    }
                }
            } else {
                try {
                    const data = await AnliModel.createAnliDongdan(req);
                    const user = await UserModel.getUserInfo(req.userId)
                    ctx.response.status = 200;
                    ctx.body = {
                        code: 200,
                        msg: '创建动弹成功',
                        data,
                        user
                    }

                } catch (err) {
                    ctx.response.status = 412;
                    ctx.body = {
                        code: 412,
                        msg: '创建动弹失败',
                        data: {
                            err: err
                        }
                    }
                }
            }

        } else {
            ctx.response.status = 416;
            ctx.body = {
                code: 416,
                msg: '参数不齐全'
            }
        }
    }

    /**
     * 动弹查询
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async getDongdan(ctx) {
        //接收客服端
        const data = await AnliModel.getDongdan();
        ctx.response.status = 200;
        ctx.body = {
            code: 200,
            msg: '查询动弹成功',
            data
        }
    }


    /**
     * 安利查询
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async getAnli(ctx) {
        let req = ctx.request.query;
        //接收客服端
        const data = await AnliModel.getAnli(req.userId);
        ctx.response.status = 200;
        ctx.body = {
            code: 200,
            msg: '查询动弹成功',
            data
        }
    }

    /**
     * 收藏点赞安利
     * @param ctx
     * @returns {Promise.<void>}
     */
    static async loveAnli(ctx) {
        let req = ctx.request.query;
        //接收客服端
        const isExsit = await AnliModel.searchLoveAnli(req);
        if (!!isExsit == false) {
            const data = await AnliModel.loveAnli(req);
            ctx.response.status = 200;
            ctx.body = {
                code: 200,
                msg: '点赞成功',
                data,
                isExsit
            }
        } else {
            ctx.response.status = 201;
            ctx.body = {
                code: 201,
                msg: '已点赞',
            }
        }
    }

}


module.exports = anliController;