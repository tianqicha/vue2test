/*
 * @Description: oss
 * @Author:gyc
 * @Date: 2019-10-02 12:30:26
 * @LastEditTime: 2019-10-02 12:41:05
 * @LastEditors: Please set LastEditors
 */
const Router = require('koa-router')
const router = new Router({
    prefix: '/oss/v1'
});
const oss = require('../config/oss')
router.get('/video', async (ctx, next) => {
    const videoName=ctx.request.query.name
    let test =await oss.myDir(videoName)
    ctx.body=test
})

module.exports = router