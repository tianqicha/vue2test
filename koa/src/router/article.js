/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-12 21:35:46
 * @LastEditTime: 2019-09-17 11:10:16
 * @LastEditors: Please set LastEditors
 */
const Router = require('koa-router')
const q = require('../config/mysql')
// var fs = require("fs");
const ArticleController = require('../controllers/article');
const router = new Router({
    prefix: '/api/v1'
  });
  router.post('/write', ArticleController.create)
  router.get('/read', ArticleController.readArtcile)
  router.get('/artlist', ArticleController.artcileList)
  router.get('/artcount', ArticleController.artcileCount)
  router.get('/artisee', ArticleController.artcileIsee)
  router.get('/artlove', ArticleController.articleLove)
  router.get('/myartlist', ArticleController.getMyArticle)
  router.get('/myartlistbom', ArticleController.getMyArticleList)
  router.get('/getMyArticleMain', ArticleController.getMyArticleMain)
  router.post('/updateMyArticle', ArticleController.updateMyArticle)
  router.get('/getMyAllArticle', ArticleController.getMyAllArticle)
  router.get('/deleteLove', ArticleController.deleteLove)
// //文章
// router.post("/write", async (ctx, next) => {
//     let artsql = "select count(id) as artcount from mo_article where owner='2' and islock=0"
//     let artcount = await q(artsql)
//     let owner = 2
//     console.log(artcount)
//     if (artcount[0].artcount < 3) {
//         const iclass = ctx.request.body.data.iclass;
//         const imoshi = ctx.request.body.data.imoshi;
//         const ijianjie = ctx.request.body.data.ijianjie
//         const igaiyao = ctx.request.body.data.igaiyao
//         const ititle = ctx.request.body.data.ititle
//         const artid = ctx.request.body.data.artid
//         // const timer = new Date().getTime()
//         const timer2 = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()
//         const timer3 = timer2 + " " + new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds()
//         let article = ctx.request.body.data.article
//         // let filePath = 'upload/' + owner + '/' + timer + '.txt'
//         let sql = "insert into mo_article values (0,'" + article + "','" + owner + "','" + timer2 + "','" + timer3 + "','1','0','" + iclass + "','" + imoshi + "','" + ijianjie + "','" + igaiyao + "','" + ititle + "',0,'" + artid + "')";

//         async function write() {
//             try {
//                 let data = await q(sql)
//                 return data

//             } catch (e) {
//                 console.log("错误，请检查！")
//             }
//         }
//         let artcieCode=await write()
//         if (!!artcieCode) {
//             ctx.response.body = '200'
//         } else {
//             ctx.response.body = '201'
//         }
//         // fs.exists("upload/" + owner, function (exists) {
//         //     if (exists) {
//         //         fs.writeFile(filePath, article, function (err) {
//         //             if (err) {
//         //                 return console.error(err);
//         //             }
//         //             console.log("数据写入成功！");
//         //             let sql = "insert into mo_article values (0,'" + filePath + "','" + owner + "','" + timer2 + "','" + timer3 + "','1','0')";
//         //             q(sql)
//         //         });
//         //     } else {
//         //         fs.mkdir("upload/" + owner, function (err) {

//         //             if (err) {
//         //                 return console.error(err);
//         //             }
//         //             fs.writeFile(filePath, article, function (err) {
//         //                 if (err) {
//         //                     return console.error(err);
//         //                 }
//         //                 console.log("数据写入成功！");
//         //                 let sql = "insert into mo_article values (0,'" + filePath + "','" + owner + "','" + timer2 + "','" + timer3 + "','1','0')";
//         //                 q(sql)
//         //             });
//         //         })
//         //     }
//         // })

//     } else {
//         ctx.response.body = '416'
//     }
// })

// //文章
// router.get('/read', async (ctx, next) => {
//     let artid = ctx.request.query.artid
//     let sql = "select a.id,b.user,a.article,a.ititle,a.isee from mo_article a left join mo_user b on a.owner=b.id where a.id=" + artid
//     let data = await q(sql)
//     // function a() {
//     //     return new Promise((resolve, reject) => {
//     //         fs.readFile(data[0].article, (err, data) => {
//     //             if (err) {
//     //                 return console.error(err);
//     //             }
//     //             console.log("异步读取文件数据: " + data.toString());
//     //             resolve(data.toString())
//     //         });

//     //     })
//     // }
//     // async function b() {
//     //     let c = await a()
//     //     return c
//     // }
//     // let d = await b()

//     ctx.response.body = data[0]
// })

// //文章列表
// router.get('/artlist', async (ctx, next) => {
//     let page = ctx.request.query.page * 10
//     let sql = "select * from mo_article where ishow='1' and islock='1' limit " + page + ",10"
//     let data = await q(sql)
//     let sql2 = "select * from mo_article where ishow='1' and islock='1'"
//     let data2 = await q(sql2)
//     ctx.response.body = { data: data, data2: data2.length }
// })

// //浏览量
// router.get('/artsee', async (ctx, next) => {
//     let artid = ctx.request.query.artid
//     let sql = "update mo_article set isee=isee+1 where id="+artid
//     let data = await q(sql)
//     ctx.response.body = data
// })
module.exports = router