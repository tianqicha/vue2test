const Router = require('koa-router')

const ReviewController = require('../controllers/review');

const router = new Router({
  prefix: '/api/v1'
});

router.get('/review', ReviewController.create)

router.get('/getReview', ReviewController.getReview)

router.get('/destroyReview', ReviewController.destroyReview)

module.exports = router