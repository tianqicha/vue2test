const Router = require('koa-router')

const AnliController = require('../controllers/anli');

const router = new Router({
  prefix: '/api/v1'
});

router.get('/createDongdan', AnliController.create)
router.get('/getDongdan', AnliController.getDongdan)
router.get('/getAnli', AnliController.getAnli)
router.get('/loveAnli', AnliController.loveAnli)


module.exports = router