/*
 * @Description: In User Settings Edit
 * @Author: gyc
 * @Date: 2019-09-12 21:35:46
 * @LastEditTime: 2019-09-17 17:43:59
 * @LastEditors: Please set LastEditors
 */
const Router = require('koa-router')
// const router = new Router()
// const q = require('../config/mysql')
const UserController = require('../controllers/user');
const oss = require('../config/oss')
var fs = require("fs");
const router = new Router({
  prefix: '/api/v1'
});
router.get('/register', UserController.create) //注册用户
router.get('/testuser', UserController.testUser) //检测用户名
router.get('/login', UserController.login) //登录
router.get('/nowlogin', UserController.nowlogin) //登录状态
router.get('/userinfo', UserController.userinfo)
router.get('/getUserinfoo', UserController.getUserinfoo)
router.get('/createUserinfo', UserController.createUserinfo)
router.get('/exit', async (ctx, next) => {
  ctx.session.userinfo = "";
  ctx.response.status = 200;
  ctx.body = {
    code: 200,
    msg: '退出成功',
  }
//   ctx.cookies.set('loginfo',  {
//     id: -1,
//     status: false
// }, {
//     maxAge: 3600 * 24
//   })
})

router.post('/upload', async (ctx, next) => {
  ctx.body = {
    code: 200,
    msg: '上传成功'
  };
  console.log(ctx.request.files.file)
  let rs=fs.createReadStream(ctx.request.files.file.path)
  console.log(rs)
  // console.log(a)
  oss.put(ctx.request.body.name, rs)
})


router.post('/post', async (ctx, next) => {

  console.log('postParam')
})

module.exports = router