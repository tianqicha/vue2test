/*
 * @Description: In User Settings Edit
 * @Author: gyc
 * @Date: 2019-09-09 21:37:49
 * @LastEditTime: 2020-02-21 13:58:55
 * @LastEditors: Please set LastEditors
 */
const Koa = require('koa')
const app = new Koa()
// const path = require('path')
// const cors = require('koa-cors');
const session = require('koa-session');
const koaBody = require('koa-body');
const RedisStore = require('koa2-session-ioredis');
const { accessLogger, systemLogger, } = require('./config/logger');
// const user = require('./router/user')
// const article = require('./router/article')
// const oss = require('./router/oss')
// const review = require('./router/review')
// const anli = require('./router/anli')
// const oss=require('./config/oss')
// app.use(cors())//cookie
//跨域支持cookie
app.use(async (ctx, next) => {
    ctx.set('Access-Control-Allow-Credentials', 'true')
    ctx.set('Access-Control-Allow-Origin', ctx.headers["origin"] || '*');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    ctx.set('Content-Type', 'application/json;charset=utf-8')
    if (ctx.method == 'OPTIONS') {
        ctx.body = 200;
    } else {
        await next();
    }
});

// oss.listDir('sao.mp4')
app.keys = ['keys', 'keykeys'];
app.use(session({
  key: 'koa:sess',
  maxAge: 3600*1000*1000,
  overwrite: true, 
  httpOnly: true, 
  signed: true, 
  rolling: false,
  renew: false, 
  store: new RedisStore({
    // Options specified here
    // all `ioredis` options
    host: '47.94.244.199',
    ttl: 3600*1000*1000,
    port: 6379,
    logErrors: true
  })
}, app));

//redis模板
// const redis = require("./redis/redis") // 你的redis配置文件路径
// redis.set('sessionId','gyc')  //account为登录帐号信息，需要自行设置
// redis.get('sessionId').then(function (result) {
//     console.log(result)
// })

// 添加路由
app.use(koaBody({
  jsonLimit:1024000000,
  textLimit:1024000000,
  formLimit:1024000000,
  multipart: true,
}))
// app.use(user.routes())
// app.use(article.routes())
// app.use(oss.routes())
// app.use(review.routes())
// app.use(anli.routes())
app.use(accessLogger());
// app.on('error', err => { logger.error(err); });

app.listen(3000)
console.log('[demo] start-quick is starting at port 3000')