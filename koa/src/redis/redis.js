const redis = require('ioredis');

const config = {
    port: 6379,          // Redis port
    // host: 'localhost',   // Redis host
    host: '47.94.244.199',   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    password: '',
    db: 0
}

const newredis = new redis(config);

module.exports = newredis;